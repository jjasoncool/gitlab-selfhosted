# Setup #
You need to install docker and docker-compose first.

Before using docker-compose, you need to create these folders `mkdir -p config logs data`.

Do `sh setup.sh`

若沒有需要使用 ssl 加密，直接執行 docker-compose up -d 即可

### config file
You can use the `cp ./gitlab.rb.template ./config/gitlab.rb` after docker-compose up and restart containers.

# initial login #
root password : `sudo docker exec -it gitlab grep 'Password:' /etc/gitlab/initial_root_password`
